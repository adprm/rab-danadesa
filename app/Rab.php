<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rab extends Model
{

    public $timestamps = true;
    protected $table = 'tbl_hrab';

    protected $fillable = ['id_hrab','desa','kecamatan','kabupaten','provinsi','bidang','kegiatan','panjang','lebar','tebal','volume','total_biaya'];
}
