<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\Rab;
use App\Huraian;
use App\Uraian;
use PDF;

class RabController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['jml_rab'] = DB::table('tbl_hrab')->count();
        $data['jml_jdu'] = DB::table('tbl_jduraian')->count();
    	$data['navi'] = "Dashboard";
        return view('content/home',$data);
    }

    //=============================== header RAB
    public function listRab()
    {
    	$data['navi'] = "List Rab";
        $data['Rab'] = Rab::all();
        return view('content/list',$data);
    }

   	public function addRab()
    {
        $data['Uraian'] = Uraian::all();
        $data['navi'] 	= "Tambah Rab";
        return view('content/addHeaderRab',$data);
    }

    public function storeRab(Request $request)
    {

    	$id = DB::table('tbl_hrab')->orderBy('id_hrab','desc')->first('id_hrab');
        if (empty($id)) {
                $da = 0;
        }else{
            foreach($id as $r){
                $da = $r;
            }    
        }
        $messages = [
            'required' => 'Data :attribute wajib diisi!',
            'numeric' => 'Data :attribute harus menggunakan angka!',
        ];
        $this->validate($request,[
            'desa' 		=> 'required',
            'kecamatan' => 'required',
            'kabupaten' => 'required',
            'provinsi' 	=> 'required',
            'bidang' 	=> 'required',
            'kegiatan' 	=> 'required',
            'panjang' 	=> 'required|numeric',
            'lebar' 	=> 'required|numeric',
            'tebal' 	=> 'required|numeric',
            
        ],$messages);
        $idj = 1;
        $idh = $da + 1;
        $data = array(
            'id_hrab' 	=> $idh,
            'desa' 		=> $request->desa,
            'kecamatan' => $request->kecamatan,
            'kabupaten' => $request->kabupaten,
            'provinsi' 	=> $request->provinsi,
            'bidang' 	=> $request->bidang,
            'kegiatan' 	=> $request->kegiatan,
            'panjang' 	=> $request->panjang,
            'lebar' 	=> $request->lebar,
            'tebal' 	=> $request->tebal,
        	'volume' 	=> $request->volume,
        	'total_biaya'=> 0);
        Rab::create($data);
        
        return redirect('/Uraian/'.$idj.'/'.$idh);
    }

    public function updateRab(Request $request)
    {
        $data = array(
            'desa'      => $request->desa,
            'kecamatan' => $request->kecamatan,
            'kabupaten' => $request->kabupaten,
            'provinsi'  => $request->provinsi,
            'bidang'    => $request->bidang,
            'kegiatan'  => $request->kegiatan,
            'panjang'   => $request->panjang,
            'lebar'     => $request->lebar,
            'tebal'     => $request->tebal,
            'volume'    => $request->volume,
            'total_biaya' => 0);

        Rab::where('id_hrab','=',$request->id_hrab)
        ->update($data);
        
        // =================================
        $data['sum']        = DB::table('tbl_huraian')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([
                                ['tbl_huraian.id_hrab','=',$request->id_hrab],
                                ['tbl_huraian.id_jduraian','=',$request->id_jduraian]])
                                ->sum('jumlah');


        $data['tb']         = DB::table('tbl_huraian')
                                ->where('id_hrab','=',$request->id_hrab)
                                ->sum('sub_total');

        Rab::where('id_hrab','=',$request->id_hrab)
        ->update(['total_biaya' => $data['tb']]);    
        
        DB::table('tbl_huraian')
        ->where([['tbl_huraian.id_hrab','=',$request->id_hrab],
                ['tbl_huraian.id_jduraian','=',$request->id_jduraian]])
        ->update(['sub_total' => $data['sum']]);
        // =================================

        return redirect('/Uraian/'.$request->id_jduraian.'/'.$request->id_hrab);
    }

    public function deleteRab($id)
    {
        DB::table('tbl_hrab')->where('id_hrab','=',$id)->delete();
        return redirect('/Rab');
    }

    //=============================== header RAB
    //=============================== Uraian RAB

    public function totalAll($id2){
        $tbiaya         = DB::table('tbl_huraian')
                                ->where('id_hrab','=',$id2)
                                ->sum('sub_total');
        Rab::where('id_hrab','=',$id2)->update(['total_biaya' => $tbiaya]);
        return $tbiaya;
    }

    public function addUraian($id1,$id2)
    {   
        $data['hrab']       = DB::table('tbl_hrab')->where('id_hrab','=',$id2)->first();
        $data['id_jduraian']= $id1;
        $data['id_hrab']    = $id2;
        $data['nm_jd']      = DB::table('tbl_jduraian')->where('id_jduraian','=',$id1)->first('nm_jd');
        $data['jdUraian']   = DB::table('tbl_jduraian')->get();
        $data['Uraian']     = DB::table('tbl_huraian')
                                ->select('tbl_uraian.id_uraian','tbl_huraian.id_huraian','uraian','volume','satuan','hsatuan','jumlah')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([
                                ['tbl_huraian.id_hrab','=',$id2],
                                ['tbl_huraian.id_jduraian','=',$id1]])
                                ->get();
        $idhu                 = DB::table('tbl_huraian')->orderBy('id_huraian','desc')->first('id_huraian');
        if (empty($idhu)) {
                $da = 0;
        }else{
            foreach($idhu as $r){
                $da = $r;
            }   
        }
        $datahuraian        = DB::table('tbl_huraian')
                            ->select('id_huraian')
                            ->where([
                            ['tbl_huraian.id_hrab','=',$id2],
                            ['tbl_huraian.id_jduraian','=',$id1]])
                            ->first();
        if(!empty($datahuraian)){
            foreach($datahuraian as $dthu){
                $idh =  $dthu;
            }
        }else{
            $idh = $da + 1;
        }
        $data['sum']        = DB::table('tbl_huraian')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([
                                ['tbl_huraian.id_hrab','=',$id2],
                                ['tbl_huraian.id_jduraian','=',$id1]])
                                ->sum('jumlah');

        // $data['tb']         = DB::table('tbl_huraian')
        //                         ->where('id_hrab','=',$id2)
        //                         ->sum('sub_total');

        // //Insert total biaya di tbl_hrab
        // // $data['tb']         = DB::table('tbl_huraian')
        // //                         ->where('id_hrab','=',$id2)
        // //                         ->sum('sub_total');
        // Rab::where('id_hrab','=',$id2)
        // ->update(['total_biaya' => $data['tb']]);    
        // //Insert total biaya di tbl_hrab

        // $data['tb']         = DB::table('tbl_huraian')
        //                         ->where('id_hrab','=',$id2)
        //                         ->sum('sub_total');
        
        DB::table('tbl_huraian')
        ->where([['tbl_huraian.id_hrab','=',$id2],
                ['tbl_huraian.id_jduraian','=',$id1]])
        ->update(['sub_total' => $data['sum']]);

        $data['id_huraian'] = $idh;
        $data['navi']       = "Tambah Uraian";
        return view('content/uraian',$data);
    }

    public function storeUraian(Request $request)
    {
        $id = DB::table('tbl_huraian')->where([
            ['id_jduraian','=',$request->id_jduraian],
            ['id_hrab','=',$request->id_hrab],
        ])->count();

            //================== insert huraian
            $data = array(
                'id_huraian'    => $request->id_huraian,
                'id_hrab'       => $request->id_hrab,
                'id_jduraian'   => $request->id_jduraian,
                'sub_total'     => $request->sub_total
                );
            
            //================== insert huraian
        
            $idur                = DB::table('tbl_uraian')->orderBy('id_uraian','desc')->first('id_uraian');
            if (empty($idur)) {
                    $du = 1;
            }else{
                foreach($idur as $r){
                    $du = $r + 1;
                }   
            }
            if ($request->id_jduraian == 5) {
                $messages = [
                'required' => 'Data :attribute wajib diisi!',
                'numeric' => 'Data :attribute harus menggunakan angka!',
                ];
                $this->validate($request,['sub_total' => 'required|numeric'],$messages);

                $uraian = $request->uraian;
                $result = array();
                foreach($uraian as $key => $val){
                   $result[] = array(
                        'id_huraian'=> $request->id_huraian,
                        'id_uraian' => $du++,
                        'uraian'    => $request->uraian[$key],
                        'volume'    => 1,
                        'satuan'    => "Orang",
                        'hsatuan'   => $request->hsatuan[$key],
                        'jumlah'    => $request->hsatuan[$key] 
                    );
                }
                if ($id==0) {
                    Huraian::create($data);
                    Uraian::insert($result);
                }else{
                    DB::table('tbl_huraian')->where([
                    ['id_hrab','=',$request->id_hrab],
                    ['id_jduraian','=',$request->id_jduraian]
                    ])->delete();
                    Huraian::create($data);
                    Uraian::insert($result);
                }
            }else{
                 $uraian = $request->uraian;
                $result = array();
                foreach($uraian as $key => $val){

                $messages = [
                    'required' => 'Data :attribute wajib diisi!',
                    'numeric' => 'Data :attribute harus menggunakan angka!',
                    ];
                $this->validate($request,[
                    'uraian.*'   => 'required',
                    'volume.*'   => 'required|numeric',
                    'satuan.*'   => 'required',
                    'hsatuan.*'  => 'required|numeric',
                    'jumlah.*'   => 'required|numeric',
                ],$messages);

                   $result[] = array(
                        'id_huraian'=> $request->id_huraian,
                        'id_uraian' => $du++,
                        'uraian'    => $request->uraian[$key],
                        'volume'    => $request->volume[$key],
                        'satuan'    => $request->satuan[$key],
                        'hsatuan'   => $request->hsatuan[$key],
                        'jumlah'    => $request->hsatuan[$key] * $request->volume[$key]  
                    );
                }
                if ($id==0) {
                Huraian::create($data);
                Uraian::insert($result);
                }else{
                    Uraian::insert($result);
                }
            }
        $this->totalAll($request->id_hrab);
        return redirect('/Uraian/'.$request->id_jduraian.'/'.$request->id_hrab);
    }

    public function deleteUraian($id,$id1,$id2)
    {
        $dltDt = DB::table('tbl_uraian')->where('id_uraian','=',$id)->delete();
        // $this->totalAll($id2);
        return redirect('/Uraian/'.$id1.'/'.$id2);
    }

    //=============================== Uraian RAB  
    //=============================== Cetak RAB  
    // public function reportPdf($id)
    // {
    //     $data['rab']        = DB::table('tbl_hrab')->where('id_hrab','=',$id)->get();
    //     // $data['jdUraian']   = DB::table('tbl_jduraian')->get();

    //     $data['tes1']     = DB::table('tbl_huraian')
    //                             ->select('tbl_huraian.id_jduraian as id','tbl_jduraian.nm_jd as nama')
    //                             ->leftJoin('tbl_jduraian', 'tbl_huraian.id_jduraian', '=', 'tbl_jduraian.id_jduraian')
    //                             ->where('tbl_huraian.id_hrab','=',$id)
    //                             ->get();
    //     $data['uraianjd']     = DB::table('tbl_huraian')
    //                             ->select('tbl_huraian.id_jduraian','uraian','volume','satuan','hsatuan','jumlah','sub_total')
    //                             ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
    //                             ->where('tbl_huraian.id_hrab','=',$id)
    //                             ->get();
    //     $pdf        = PDF::loadview('content/rab_pdf',$data);
    //     return $pdf->stream();
    // }  
    
    public function reportPdf($id)
    {
        $data['rab']        = DB::table('tbl_hrab')->where('id_hrab','=',$id)->get();
        $data['jdUraian']   = DB::table('tbl_jduraian')->get();

        $data['uraianjd1']     = DB::table('tbl_huraian')
                                ->select('uraian','volume','satuan','hsatuan','jumlah','sub_total')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','1']])
                                ->get();
        $data['subtt1']     = DB::table('tbl_huraian')
                                ->select('sub_total')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','1']])
                                ->first();
        $data['uraianjd2']     = DB::table('tbl_huraian')
                                ->select('uraian','volume','satuan','hsatuan','jumlah')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','2']])
                                ->get();
        $data['subtt2']     = DB::table('tbl_huraian')
                                ->select('sub_total')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','2']])
                                ->first();
        $data['uraianjd3']     = DB::table('tbl_huraian')
                                ->select('uraian','volume','satuan','hsatuan','jumlah')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','3']])
                                ->get();
        $data['subtt3']     = DB::table('tbl_huraian')
                                ->select('sub_total')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','3']])
                                ->first();
        $data['uraianjd4']     = DB::table('tbl_huraian')
                                ->select('uraian','volume','satuan','hsatuan','jumlah')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','4']])
                                ->get();
        $data['subtt4']     = DB::table('tbl_huraian')
                                ->select('sub_total')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','4']])
                                ->first();
        $data['uraianjd5']     = DB::table('tbl_huraian')
                                ->select('uraian','volume','satuan','hsatuan','jumlah')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','5']])
                                ->get();
        $data['subtt5']     = DB::table('tbl_huraian')
                                ->select('sub_total')
                                ->leftJoin('tbl_uraian', 'tbl_huraian.id_huraian', '=', 'tbl_uraian.id_huraian')
                                ->where([['tbl_huraian.id_hrab','=',$id],
                                        ['tbl_huraian.id_jduraian','=','5']])
                                ->first();
        $data['tb']         = DB::table('tbl_hrab')
                                ->leftJoin('tbl_huraian', 'tbl_hrab.id_hrab', '=', 'tbl_huraian.id_hrab')
                                ->where('tbl_huraian.id_hrab','=',$id)
                                ->sum('sub_total');
        $pdf        = PDF::loadview('content/rab_pdf2',$data);
        return $pdf->stream();
    }  
    //=============================== Cetak RAB  
}
