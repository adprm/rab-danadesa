<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

use App\jdUraian;

class JdUraianController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function listUJd()
    {
    	$data['navi'] = "List Judul Uraian";
        $data['jduraian'] = DB::table('tbl_jduraian')->where('id_jduraian','!=',5)->get();
        return view('content/jdUraian/list',$data);
    }

   	public function addUJd()
    {
        $data['navi'] 	= "Tambah Judul Uraian";
        return view('content/jdUraian/add',$data);
    }

    public function storeUJd(Request $request)
    {
        $this->validate($request,[
            'nm_jd' => 'required'
        ]);

        $id = DB::table('tbl_jduraian')->orderBy('id_jduraian','desc')->first('id_jduraian');
        foreach ($id as $r) {
           $id2 = $r+1;
        }
        $data = array(
            'id_jduraian' => $id2,
            'nm_jd' => $request->nm_jd
        );
        jdUraian::create($data);
        
        return redirect('/JudulUraian');
    }

    public function EditUJd($id)
    {
        $data['navi']   = "Edit Judul Uraian";
        $data['jduraian'] = DB::table('tbl_jduraian')
                            ->where('id_jduraian','=',$id)
                            ->get();
        
        return view('content/jdUraian/edit',$data);
    }

    public function UpdateUJd(Request $request)
    {
        jdUraian::where('id_jduraian','=',$request->id_jd)
        ->update(['nm_jd' => $request->nm_jd]);

        return redirect('/JudulUraian');
    }

    public function deleteUJd($id)
    {
        DB::table('tbl_jduraian')->where('id_jduraian','=',$id)->delete();
        return redirect('/JudulUraian');
    }
}
