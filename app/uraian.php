<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class uraian extends Model
{
	public $timestamps = true;
    protected $table = 'tbl_uraian';

    protected $fillable = ['id_huraian','id_uraian','uraian','volume','satuan','hsatuan','jumlah'];
}
