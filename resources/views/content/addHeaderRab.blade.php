@extends('layouts.app')

@section('content')
  <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">RAB</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              
              <div class="card-body">
                  @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
                <form action="/Rab/InsertRab" class="form-horizontal" method="post">
                  {{ csrf_field() }}
                 
                  <div class="row">

                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="form-group row">
                          <div class="col-sm-10">
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <div class="col-md-6">
                      <!-- /.form-group -->
                      <div class="form-group">
                            <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Desa</label>
                        <div class="col-sm-10">
                          <input type="text" name="desa" class="form-control"  placeholder="Desa"  >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Kecamatan</label>
                        <div class="col-sm-10">
                          <input type="text" name="kecamatan" class="form-control"  placeholder="Kecamatan"  >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Kabupaten</label>
                        <div class="col-sm-10">
                          <input type="text" name="kabupaten" class="form-control"  placeholder="Kabupaten"  >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Provinsi</label>
                        <div class="col-sm-10">
                          <input type="text" name="provinsi" class="form-control"  placeholder="Provinsi"  >
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                          <input type="text" name="bidang" class="form-control"  placeholder="bidang"  >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Kegiatan</label>
                        <div class="col-sm-10">
                          <input type="text" name="kegiatan" class="form-control"  placeholder="Kegiatan" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Volume</label>
                        <div class="col-sm-2">
                          <input type="text" name="panjang" id="panjang" class="form-control" placeholder="Panjang" >
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="lebar" id="lebar" class="form-control"  placeholder="Lebar" >
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="tebal" id="tebal" class="form-control"  placeholder="Tebal" >
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="volume" id="volume" class="form-control"  placeholder="Volume" readonly>
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                            <div class="form-group row">
                        <div class="col-sm-2">
                          <br>
                          <input type="submit" class="btn btn-primary"  value="Simpan" >
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  </form>
              </div>
            
              <!-- /.card-body -->
              <div class="card-footer">
                <!-- Footer -->
              </div>
              <!-- /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
@endsection