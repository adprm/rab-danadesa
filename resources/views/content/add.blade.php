@extends('layouts.app')

@section('content')
  <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">RAB</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              
              <div class="card-body">
                <form action="/Rab/UpdateRab" class="form-horizontal" method="post">
                  {{ csrf_field() }}
                  <input type="hidden" name="id_jduraian" value="{{$id_jduraian}}">
                  <input type="hidden" name="id_hrab" value="{{$id_hrab}}">
                  <?php
                 if (empty($hrab)) {
                    echo "data tidak tersedia";
                  }else{
                  ?>
                  <div class="row">

                    <div class="col-md-12">
                      <div class="form-group">
                        <div class="form-group row">
                          <div class="col-sm-10">
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <div class="col-md-6">
                      <!-- /.form-group -->
                      <div class="form-group">
                            <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Desa</label>
                        <div class="col-sm-10">
                          <input type="text" name="desa" class="form-control"  placeholder="Desa" value="{{$hrab->desa}}" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Kecamatan</label>
                        <div class="col-sm-10">
                          <input type="text" name="kecamatan" class="form-control"  placeholder="Kecamatan" value="{{$hrab->kecamatan}}" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Kabupaten</label>
                        <div class="col-sm-10">
                          <input type="text" name="kabupaten" class="form-control"  placeholder="Kabupaten" value="{{$hrab->kabupaten}}" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Provinsi</label>
                        <div class="col-sm-10">
                          <input type="text" name="provinsi" class="form-control"  placeholder="Provinsi" value="{{$hrab->provinsi}}" >
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                    <div class="col-md-6">
                      <div class="form-group">
                            <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">No.RAB</label>
                        <div class="col-sm-10">
                          <input type="text" name="id_hrab" class="form-control"  placeholder="No.RAB" value="{{$hrab->id_hrab}}" disabled>
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Bidang</label>
                        <div class="col-sm-10">
                          <input type="text" name="bidang" class="form-control"  placeholder="bidang" value="{{$hrab->bidang}}" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Kegiatan</label>
                        <div class="col-sm-10">
                          <input type="text" name="kegiatan" class="form-control" value="{{$hrab->kegiatan}}"  placeholder="Kegiatan" >
                        </div>
                      </div>
                      <div class="form-group row">
                        <label  class="col-sm-2 col-form-label">Volume</label>
                        <div class="col-sm-2">
                          <input type="text" name="panjang" id="panjang" class="form-control" value="{{$hrab->panjang}}"  placeholder="Panjang" >
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="lebar" id="lebar" class="form-control" value="{{$hrab->lebar}}"  placeholder="Lebar" >
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="tebal" id="tebal" class="form-control" value="{{$hrab->tebal}}"  placeholder="Tebal" >
                        </div>
                        <div class="col-sm-4">
                          <input type="text" name="volume" id="volume" class="form-control" value="<?php if (isset($hrab->volume)): ?>{{$hrab->volume}}
                            <?php endif ?>"  placeholder="Volume" readonly>
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                            <div class="form-group row">
                        <div class="col-sm-2">
                          <br>
                          <input type="submit" class="btn btn-primary"  value="Ubah" >
                        </div>
                      </div>
                      </div>
                      <!-- /.form-group -->
                    </div>
                    <!-- /.col -->
                  </div>
                  <?php
                  }
                  ?>
                  </form>
              </div>
            
              <!-- /.card-body -->
              <div class="card-footer">
                <!-- Footer -->
              </div>
              <!-- /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3">

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Judul Uraian</h3>

              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="card-body p-0">
              <ul class="nav nav-pills flex-column">
                 <?php
                  $no = 1;
                 if (empty($jdUraian)) {
                    echo "data tidak tersedia";
                  }else{ 
                    foreach($jdUraian as $r){
                      $n = $no++;
                  ?>
                <li class="nav-item active">
                  <a class="nav-link" href="/Uraian/{{$r->id_jduraian}}/{{$hrab->id_hrab}}" aria-selected="true">
                    <i class="fas fa-inbox"></i> {{$r->nm_jd}}
                  </a>
                </li>
                <?php
                  }}
                  ?>
              </ul>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
        <div class="col-md-9">
          <div class="card card-primary card-outline">
            <div class="card-header">
              @foreach($nm_jd as $r)
              <h3 class="card-title">{{$r}}</h3>
              @endforeach

              <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              <!-- /.card-tools -->
            </div>
            <!-- /.card-header -->
            <div class="card-body">
            <!-- <h4>hrab</h4> -->
            <div class="row">
              
              <div class="col-md-12">
                <div class="tab-content" id="vert-tabs-tabContent">
                  <div class="tab-pane text-left fade show active" id="vert-tabs-1" role="tabpanel" aria-labelledby="vert-tabs-1-tab">
                    @yield('tab')
                  </div>
                </div>
              </div>
            </div>
              <!-- /.card-body -->
              
              <!-- /.card-footer-->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
              
            </div>
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
@endsection