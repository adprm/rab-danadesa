@extends('layouts.app')

@section('content')
  <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List RAB</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                    <i class="fas fa-times"></i></button>
                </div>
              </div>
              <div class="card-body">
                  <table id="dtTable" class="table table-bordered table-striped">
                    <thead>
                     <tr>
                          <th>No. RAB</th>
                          <th>Desa</th>
                          <th>Bidang</th>
                          <th>Kegiatan</th>
                          <th>Opsi</th>
                     </tr>
                  </thead>
                  <tbody>
                    <?php
                   if (empty($Rab)) {
                      echo "data tidak tersedia";
                    }else{ 
                      $no = 1;
                      foreach($Rab as $r){
                    ?>
                    <tr>
                        <td>{{$r->id_hrab}}  </td>
                        <td>{{$r->desa}}  </td>
                        <td>{{$r->bidang}}  </td>
                        <td>{{$r->kegiatan}} </td>
                        <td><a href="/Rab/cetak_pdf/{{$r->id_hrab}}" class="btn btn-success" target="_blank"><i class="fa fa-print"></i></a>
                            <a href="/Uraian/1/{{$r->id_hrab}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#modal-dlt-rab{{$r->id_hrab}}"><i class="fa fa-trash"></i></a></td>

                    </tr>
                    <?php
                      }}
                    ?>
                  </tbody>
               </table>
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
              </div>
              <!-- /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
@endsection