@extends('layouts.app')

@section('content')
  <section class="content">

      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <!-- Default box -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">RAB</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fas fa-minus"></i></button>
                </div>
              </div>
              
              <div class="card-body">
                <table id="dtTable" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                      <th>No</th>
                      <th>Nama Judul</th>
                      <th>Created at</th>
                      <th>Update at</th>
                      <th>Opsi</th>
                  </tr>
                  </thead>
                   <tbody>
                    <?php
                   if (empty($jduraian)) {
                      echo "data tidak tersedia";
                    }else{ 
                      $no = 1;
                      foreach($jduraian as $r){
                    ?>
                      <tr>
                        <td>{{$no++}}</td>
                        <td>{{$r->nm_jd}}</td>
                        <td>{{$r->created_at}}  </td>
                        <td>{{$r->updated_at}}  </td>
                        <td>
                        <a href="/JudulUraian/Edit/{{$r->id_jduraian}}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
                        <!-- <a href="" class="btn btn-danger" data-toggle="modal" data-target="#modal-dlt-jd{{$r->id_jduraian}}"><i class="fa fa-trash"></i></a></td>
                       --></tr>
                      <?php
                  }}
                  ?>
                    </tbody>
              </table>
              </div>
            
              <!-- /.card-body -->
              <div class="card-footer">
                <!-- Footer -->
              </div>
              <!-- /.card-footer-->
            </div>
            <!-- /.card -->
          </div>
        </div>
      </div>
    </section>
@endsection