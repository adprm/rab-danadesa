
<title>
 REPORT RAB
 </title>
 <?php
      foreach($rab as $r){
        $desa       = $r->desa;
        $kecamatan  = $r->kecamatan;
        $kabupatan  = $r->kabupaten;
        $provinsi   = $r->provinsi;
        $id         = $r->id_hrab;
        $bidang     = $r->bidang;
        $kegiatan   = $r->kegiatan;
        $panjang    = $r->panjang;
        $lebar      = " x ". $r->lebar;
        $tebal      = " x ". $r->tebal ? $r->tebal : '' ;
        $volume     = " = ". $r->volume;
      }
    ?>
<table>
      <tr>
          <td width="60">Desa</td>
          <td width="30">:</td>
          <td width="50">{{$desa}}</td>
          <td width="130">&nbsp;</td>
          <td width="60">No.RAB</td>
          <td width="30">:</td>
          <td width="50">{{$id}}</td>
      </tr>
      <tr>
          <td>Kecamatan</td>
          <td>:</td>
          <td>{{$kecamatan}}</td>
          <td></td>
          <td>Bidang</td>
          <td>:</td>
          <td>{{$bidang}}</td>
      </tr>
      <tr>
          <td>Kabupaten</td>
          <td>:</td>
          <td>{{$kabupatan}}</td>
          <td></td>
          <td>kegiatan</td>
          <td>:</td>
          <td>{{$kegiatan}}</td>
      </tr>
      <tr>
          <td>Provinsi</td>
          <td>:</td>
          <td>{{$provinsi}}</td>
          <td></td>
          <td>volume</td>
          <td>:</td>
          <td>{{$panjang}} {{$lebar}} {{$tebal}} {{$volume}}</td>
      </tr>
</table><br>
<table style="border-collapse: collapse;" border="1">
  <tr>
    <td width="100">URAIAN</td>
    <td width="100">Volume</td>
    <td width="80">Satuan</td>
    <td width="100">Harga Satuan</td>
    <td width="100">Jumlah</td>
  </tr>
  <tr>
    <td colspan="5">Bahan</td>
  </tr>
  <?php if(!empty($uraianjd1)){ 
    foreach($uraianjd1 as $r ){?>
  <tr>
    <td>{{$r->uraian}}</td>
    <td>{{$r->volume}}</td>
    <td>{{$r->satuan}}</td>
    <td>{{$r->hsatuan}}</td>
    <td>{{$r->jumlah}}</td>
  </tr>
  <?php }} ?>
  <tr>
    <td colspan="4">Sub Total</td>
    <?php if(!empty($subtt1)){ 
    foreach($subtt1 as $r ){?>
    <td>{{$subtt1->sub_total}}</td>
    <?php }} ?>
  </tr>
  
  
  
  <tr>
      <td colspan="5">&nbsp;<br><br></td>
  </tr>
  <tr>
    <td colspan="5">Alat</td>
  </tr>
  <?php if(!empty($uraianjd2)){ 
    foreach($uraianjd2 as $r ){?>
  <tr>
    <td>{{$r->uraian}}</td>
    <td>{{$r->volume}}</td>
    <td>{{$r->satuan}}</td>
    <td>{{$r->hsatuan}}</td>
    <td>{{$r->jumlah}}</td>
  </tr>
  <?php }} ?>
  <tr>
    <td colspan="4">Sub Total</td>
    <?php if(!empty($subtt2)){ 
    foreach($subtt2 as $r ){?>
    <td>{{$subtt2->sub_total}}</td>
    <?php }} ?>
  </tr>
  
  
  
  <tr>
      <td colspan="5">&nbsp;<br><br></td>
  </tr>
  <tr>
    <td colspan="5">Upah</td>
  </tr>
  <?php if(!empty($uraianjd3)){ 
    foreach($uraianjd3 as $r ){?>
  <tr>
    <td>{{$r->uraian}}</td>
    <td>{{$r->volume}}</td>
    <td>{{$r->satuan}}</td>
    <td>{{$r->hsatuan}}</td>
    <td>{{$r->jumlah}}</td>
  </tr>
  <?php }} ?>
  <tr>
    <td colspan="4">Sub Total</td>
    <?php if(!empty($subtt3)){ 
    foreach($subtt3 as $r ){?>
    <td>{{$subtt3->sub_total}}</td>
    <?php }} ?>
  </tr>
  
  
  
  <tr>
      <td colspan="5">&nbsp;<br><br></td>
  </tr>
  <tr>
    <td colspan="5">ATK</td>
  </tr>
  <?php if(!empty($uraianjd4)){ 
    foreach($uraianjd4 as $r ){?>
  <tr>
    <td>{{$r->uraian}}</td>
    <td>{{$r->volume}}</td>
    <td>{{$r->satuan}}</td>
    <td>{{$r->hsatuan}}</td>
    <td>{{$r->jumlah}}</td>
  </tr>
  <?php }} ?>
  <tr>
    <td colspan="4">Sub Total</td>
    <?php if(!empty($subtt4)){ 
    foreach($subtt4 as $r ){?>
    <td>{{$subtt4->sub_total}}</td>
    <?php }} ?>
  </tr>
  
  
  
  <tr>
      <td colspan="5">&nbsp;<br><br></td>
  </tr>
  <tr>
    <td colspan="5">TPK</td>
  </tr>
  <?php if(!empty($uraianjd5)){ 
    foreach($uraianjd5 as $r ){?>
  <tr>
    <td>{{$r->uraian}}</td>
    <td>{{$r->volume}}</td>
    <td>{{$r->satuan}}</td>
    <td>{{$r->hsatuan}}</td>
    <td>{{$r->jumlah}}</td>
  </tr>
  <?php }} ?>
  <tr>
    <td colspan="4">Sub Total</td>
    <?php if(!empty($subtt5)){ 
    foreach($subtt5 as $r ){?>
    <td>{{$subtt5->sub_total}}</td>
    <?php }} ?>
  </tr>
  
  <tr>
      <td colspan="5">&nbsp;<br><br></td>
      
  </tr>
  <tr>
    <td colspan="4">Total Biaya</td>
    <?php if(!empty($tb)){?>
    <td>{{$tb}}</td>
    <?php } ?>
  </tr>
  
</table>