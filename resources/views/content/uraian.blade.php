@extends('content.add')

@section('tab')
<div class="container">
  <div class="panel panel-default">
    <div class="panel-body">
      {{-- <div class="col-sm-4">
          <input type="text" name="" id="volume" class="form-control"  placeholder="Volume" value="{{$tb}}" readonly>
            </div> --}} 
                       
      @if (count($errors) > 0)
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif    
      <?php if ($id_jduraian==5) {
        ?>
        <form action="/Uraian/InsertUraian" method="POST">
        <br><br>  
        <input type="hidden" name="id_jduraian" value="{{$id_jduraian}}">
        <input type="hidden" name="id_hrab" value="{{$id_hrab}}">
        <input type="hidden" name="id_huraian" value="{{$id_huraian}}">
       {{ csrf_field() }}
       <input type="text" name="sub_total" id="sub_total" class="form-control" placeholder="Masukan Jumlah TPK" value="{{$sum}}">
       <?php 
          $jml[1] = 0;
          $jml[2] = 0;
          $jml[3] = 0;
          $jml[4] = 0;
          $jml[5] = 0; 
       if (!empty($Uraian)) {
          $no = 1;
          foreach ($Uraian as $r) {
          $jml[$no++] = $r->jumlah;
          } 
       }
        ?>
       <br><br>  
        <table class="table table-bordered">
        <tr>
            <td>Uraian</td>
            <td>Volume</td>
            <td>Satuan</td>
            <td>Jumlah</td>
        </tr>
        <tr>
            <td><input type="text" name="uraian[]" class="form-control" value="Ketua TPK"></td>
            <td>Orang</td>
            <td>1</td>
            <td><input type="text" name="hsatuan[]" id="ketua" class="form-control" value="{{$jml[1]}}" readonly></td>
         </tr>
         <tr>
            <td><input type="text" name="uraian[]" class="form-control" value="Sekertaris TPK"></td>
            <td>Orang</td>
            <td>1</td>
            <td><input type="text" name="hsatuan[]" id="sekertaris" class="form-control" value="{{$jml[2]}}" readonly></td>
         </tr>
         <tr>
            <td><input type="text" name="uraian[]" class="form-control" value="Anggota TPK"></td>
            <td>Orang</td>
            <td>1</td>
            <td><input type="text" name="hsatuan[]" id="anggota1" class="form-control" value="{{$jml[3]}}" readonly></td>
         </tr>
         <tr>
            <td><input type="text" name="uraian[]" class="form-control" value="Anggota TPK"></td>
            <td>Orang</td>
            <td>1</td>
            <td><input type="text" name="hsatuan[]" id="anggota2" class="form-control" value="{{$jml[4]}}" readonly></td>
         </tr>
         <tr>
            <td><input type="text" name="uraian[]" class="form-control" value="Anggota TPK"></td>
            <td>Orang</td>
            <td>1</td>
            <td><input type="text" name="hsatuan[]" id="anggota3" class="form-control" value="{{$jml[5]}}" readonly></td>
         </tr>
         <tr>
           <td colspan="4"><button class="btn btn-primary" type="submit" name="submit">Simpan</button></td>
         </tr>
    </table>
    </form>
        <?php
      }else{
       ?>
       <form action="/Uraian/InsertUraian" method="POST">
        <br><br>  
        <input type="hidden" name="id_jduraian" value="{{$id_jduraian}}">
        <input type="hidden" name="id_hrab" value="{{$id_hrab}}">
        <input type="hidden" name="id_huraian" value="{{$id_huraian}}">
        <input type="hidden" name="sub_total" value="{{$sum}}">
       {{ csrf_field() }}
       <table class="table table-bordered input-group control-group after-add-more">
            <tr>
                <td><input type="text" name="uraian[]" class="form-control" placeholder="Uraian"></td>
                <td><input type="text" id="volum" name="volume[]" class="form-control" placeholder="Volume"></td>
                <td><input type="text" name="satuan[]" class="form-control" placeholder="Satuan"></td>
                <td><input type="text" id="hsatuan" name="hsatuan[]" class="form-control" placeholder="Harga Satuan"></td>
                <td><div class="input-group-btn">
                <button class="btn btn-success add-more" type="button"><i class="glyphicon glyphicon-plus"></i>
                  <i class="fa fa-plus"></i>
                </button>
                </div>
                </td>
                </tr>
            </table>
          <div class="control-group text-center">
              <br>
              <button class="btn btn-primary" type="submit" name="submit">Simpan</button>
          </div>
    </form>
         <br><br>
         <table class="table table-bordered">
        <tr>
            <td>Uraian</td>
            <td>Volume</td>
            <td>Satuan</td>
            <td>Harga Satuan</td>
            <td>Jumlah</td>
            <td>Hapus</td>
        </tr>
         <?php
         if (empty($Uraian)) {
            echo "data tidak tersedia";
          }else{ 
            foreach($Uraian as $r){
          ?>
            <tr>
            <td>{{$r->uraian}}</td>
            <td>{{$r->volume}}  </td>
            <td>{{$r->satuan}}  </td>
            <td>{{$r->hsatuan}}  </td>
            <td>{{$r->jumlah}}   </td>
            <td><a href="/Uraian/DeleteUraian/{{$r->id_uraian}}/{{$id_jduraian}}/{{$id_hrab}}" class="btn btn-danger"><i class="fa fa-trash"></i></a></td>
            </tr>    
        <?php
        }}
        ?>
        <tr>
            <td colspan="4" class="text-right">total</td>
            <td>{{$sum}}</td>
        </tr>
    </table>
       <?php 
      } ?>
       

        <!-- Copy Fields -->
        <div class="copy d-none">
        <table class="table table-bordered control-group input-group">
            <tr>
                <td><input type="text" name="uraian[]" class="form-control" placeholder="Uraian"></td>
                <td><input type="text" name="volume[]" class="form-control" placeholder="Volume"></td>
                <td><input type="text" name="satuan[]" class="form-control" placeholder="Satuan"></td>
                <td><input type="text" name="hsatuan[]" class="form-control" placeholder="Harga Satuan"></td>
                <td><div class="input-group-btn"><button class="btn btn-danger remove" type="button">
                <i class="glyphicon glyphicon-remove"></i> <i class="fa fa-trash"></i></button>
            </div></td>
            </tr>
            </table>

        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").after(html);
      });
      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });

      $("#sub_total").keyup(function() {
            var sub_total = $("#sub_total").val();

            var jmlkt = sub_total * 30 /100;
            var jmlsk = sub_total * 25 /100;
            var jmla1 = sub_total * 15 /100;
            var jmla2 = sub_total * 15 /100;
            var jmla3 = sub_total * 15 /100;
            $("#ketua").val(jmlkt);
            $("#sekertaris").val(jmlsk);
            $("#anggota1").val(jmla1);
            $("#anggota2").val(jmla2);
            $("#anggota3").val(jmla3);
        });
    });
</script>
@endsection