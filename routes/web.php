<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth/login');
});

// Route::get('/Register', 'RegisterController@registerUser');

Auth::routes();

Route::get('/Dashboard', 'RabController@index');
Route::get('/Rab', 'RabController@listRab');
Route::get('/Rab/Add', 'RabController@addRab');
Route::post('/Rab/UpdateRab', 'RabController@updateRab');
Route::post('/Rab/InsertRab', 'RabController@storeRab');
Route::get('/Rab/Delete/{id}','RabController@deleteRab');
Route::get('/Rab/cetak_pdf/{id}', 'RabController@reportPdf');


Route::get('/Uraian/{id1}/{id2}','RabController@addUraian');
Route::post('/Uraian/InsertUraian','RabController@storeUraian');
Route::get('/Uraian/DeleteUraian/{id}/{id1}/{id2}','RabController@deleteUraian');

Route::get('/JudulUraian', 	'JdUraianController@listUJd');
Route::get('/JudulUraian/Add', 		'JdUraianController@addUJd');
Route::post('/JudulUraian/Insert', 	'JdUraianController@storeUJd');
Route::get('/JudulUraian/Edit/{id}','JdUraianController@EditUJd');
Route::post('/JudulUraian/Update', 	'JdUraianController@UpdateUJd');
Route::get('/JudulUraian/Delete/{id}','JdUraianController@deleteUJd');

